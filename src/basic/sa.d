
/*
    This module defines a deterministic tree automaton
     
    Developed by Younes Guellouma y.guellouma@lagh-univ.dz     
*/

module src.basic.sa;

import std.stdio;
import std.conv;
import std.algorithm.searching:count;
import std.algorithm.mutation:remove;


struct delta
{
    string input_state;
    string symbol;
    string output_state;
};
public class DFA
{
    private:
        //string[]symbols;
        string[] states;
        string[]f_states;
        delta[] transitions;
    public:
        int size(){return transitions.length;}
    void construct_Fst(string file)
    {
        auto F=File(file,"w+");
        if (states.length!=0){F.writeln("<Lambda> ",states[0]);}
        foreach(d;transitions)
        {
            F.writeln(d.symbol," ",d.input_state," ",d.output_state);
        }
        foreach(q;f_states)
        {
            F.writeln(q);
        }
        F.close;

    }
    void add_State(string q){states~=q;}
    void add_final_State(string q){f_states~=q;}
    void add_Rule(string i,string symb,string o)
    {
        delta _temp;
        _temp.input_state=symb;
        _temp.symbol=i;
        _temp.output_state=o;
        transitions~=_temp;
    }

    void relabel_Symbols()
    {

    }

    int count_Sigma()
{
    string [] s;
    foreach(t;this.transitions)
    {
        s~=t.symbol;
    }
    int _count=0;
    for(int i=0;i<s.length;i++)
    {
        string c=s[i];
       if ((count(s,c))>1){_count++;s[i]=to!string(i);}
    }
    return ((s.length-_count));
}

}
