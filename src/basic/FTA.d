/*

this module describes a class of FTA and all what is necessary to build a DFA associated to it.
 The minimization of this DFA in string environments produces the same stets equivalence
 classes as if the DFTA is directly minimized.
 
 Developed by Younes Guellouma y.guellouma@lagh-univ.dz 
*/


module src.basic.FTA;

import src.basic.essential;
import src.basic.sa;
import std.stdio,std.string;
import std.xml;
import std.file;
import std.algorithm.searching : canFind;
import std.algorithm;
import std.regex;
import std.conv;
import std.algorithm.comparison:equal;
import std.algorithm.setops;
import std.algorithm.comparison : equal;
import std.range;




public class FTA //the FTA class for TTSA filter
{
   private :
       string name;   //Giving a name to the automaton
       Alphabet [] alphabet ;    // Defining ranked alphabet
       State [] states;          //States list
       State [] f_states;       //Final states list
       string [] Q_bar;         //States should appear in the associated DFA.
       string [] rules;         //Transitions set
       DFA associated_Dfa;      //Contains the associated DFA which have to be exported in an appropriate format.

   public :
       this()  // a nil FTA constructor
       {
           name="Default";
       }

   //---------Methods to set and get Fta components ---------------
       void setName(string n){name=n;}
       string getName(){return name;}
       string[] getRules(){return rules;}
       Alphabet [] getAlphabet(){return this.alphabet;}
       State [] getStates(){return this.states;}
       State [] getFinalStates(){return this.f_states;}
       void setAlphabet(Alphabet [] A){this.alphabet=A;}
       void setStates(State [] S){this.states=S;}
       void setRules(string [] R){this.rules=R;}
       void setFStates(State [] S){this.f_states=S;}

  //---------A function that decides if a state is final or no based on its label (name)

       bool isFinal(State s)
       {
           bool rep=false;
           int i=0;
           string [] _temp;
           foreach(q;f_states)
           {
               _temp~=q.name;
           }
           if (canFind(_temp,s.name)){rep=true;}
           return rep;
       }

  //-------Returns a pointer to the state having a given label
       State* getStateByName(string _name)
       {
            bool n_found=true;
            int i=0;
            State* _state;
            while (n_found && i<this.states.length)
            {
                if (this.states[i].name==_name)
                    {
                       n_found=false;
                       _state=&this.states[i];
                    }else
                    {
                        i++;
                    }
            }
            return _state;
       }


 //----------A function that decides if two states have exactly the same occurrences in left side transitions
       bool are_Equivalent(string[] p,string[] q)
       {
           string[] _temp_p,_temp_q;
           _temp_p=p.dup;
           sort(_temp_p);
           //writeln(_temp_p);
           _temp_q=q.dup;
           sort(_temp_q);
           bool result=true;
           int i=0;
           if (_temp_p.length!=_temp_q.length){result=false;}
           else
           {
              result=equal(_temp_p,_temp_q);
           }
          /* while(result && i<_temp_p.length)
           {
               if (_temp_p[i]!=_temp_q[i]){result=false;}
               i++;
           }
           bool result=true;
           auto _temp=setDifference(p, q);
           if (_temp.empty){result=true;}else{result=false;}
           //return setDifference(a, b)*/
           return result;

       }

 //----------------To visualisate the Fta on shell------------------------

       void visualisate()
       {
           writeln("\n The name of the automaton is: ",getName());
           writeln("\n The alphabet is ");
           foreach(c;getAlphabet()){write("(",c.name,", ",c.rank, "), ");};
           writeln("\n\n The states set is \n");
           foreach(c;getStates()){write(" ",c.name, ", ");};
           write("\n\n The final states set is ");
           foreach(c;getFinalStates()){writeln(" ",c.name);};
           writeln("\n The transitions set is \n\n",getRules());

       }

 //----------Constructor of an fta from an xml file------------------------------
       this(string c)
       {
           string s = cast(string)std.file.read(c);
           auto xml = new DocumentParser(s);   //create an xml parser on the doc in c
           //int i=0;
           xml.onStartTag["auto"] = (ElementParser xml)
               {
                   xml.onEndTag["alphabet"]       = (in Element e) { this.alphabet=extractAlphabet(e.text); };
                   xml.onEndTag["states"]        = (in Element e) { this.states=extractStates(e.text); };
                   xml.onEndTag["Fstates"]        = (in Element e) { this.f_states=extractStates(e.text); };
                   xml.onEndTag["rule"]        = (in Element e) { this.rules~=e.text;};
                   xml.parse();
               };
      xml.parse();


       }


 //-------------------------constructor of FTA from Timbuk file------------------

this(string c,int a)
{
    // not yet achieved
    auto file=File(c,"r");
    string b;
    file.readf("%s",&b);
    string []temp=b.splitLines;
    Alphabet [] temp2=extract_Ops(temp[0]);
    this.alphabet=temp2;

}

 //----------------------Validate the constructed automaton-----------------------------

void validate()
{
    /* Validate verifies that :
      1- there is at least one leaf symbol
      2- final states are included in states list
      3- every transition is well defined, the rank and states are correct
      4- not yet achieved
      */
      bool isLeaf=false;
      foreach(s;this.alphabet)
      {
          if (s.rank==0){isLeaf=true;}
      }
      writeln(isLeaf);


}

void build_Associated_DFA()
{

    // constructs  the wanted DFA associated to this automaton
    associated_Dfa=new DFA();
    State* _state;
    string symb;
    string output;
    foreach(element;this.f_states){associated_Dfa.add_final_State(element.name);}
    foreach(element;Q_bar)
    {
        _state=getStateByName(element);
        //writeln(_state);
        associated_Dfa.add_State(_state.name);
        for (int i=0;i<_state.sigma_q.length;i++)
        {
            symb=_state.sigma_q[i];
            output=_state.output[i];
            associated_Dfa.add_Rule(symb,_state.name,output);
        }

    }
    //writeln(associated_Dfa);
   associated_Dfa.construct_Fst("exemple.fst");

   //writeln(associated_Dfa.count_Sigma());
   writeln(associated_Dfa.size());
}

//Export the tree automaton into an xml file
 
 void exportXML (){
 
  write("\n Exporting the tree automaton into an xml file ... "); 
  File file=File("aut.xml", "w+");
  file.writeln("<?xml version=\"1.0\"?>");
  file.writeln("<automaton>");
  file.write("<alphabet>");
  foreach(a; this.alphabet){file.write(a.name,",",a.rank,";");}
  file.writeln("</alphabet>");
  file.write("<states>");
  foreach(s; this.states){file.write(s.name,",");}
  file.writeln("</states>");
  file.write("<Fstates>");
  foreach(s; this.f_states){file.write(s.name,",");}
  file.writeln("</Fstates>");
  file.writeln("</delta>");  
  foreach(r; this.rules){
   file.write("\t<rule>", r);
   file.writeln("</rule>");
  }
  file.writeln("</delta>");
  file.writeln("</automaton>");
  file.close(); 
  writeln(" Done!");
 }

void export_TUK(string file)
{
    //exports the automaton in a timbuk file

    auto F=File(file,"w+");
    string _s="Ops: ";
    foreach(a;this.alphabet)
    {
        _s~=a.name~":"~to!string(a.rank)~" ";
    }
    F.writeln(_s);
    F.writeln("Automaton ",this.getName);
    _s="States ";
    foreach(q;this.states)
    {
        _s~=q.name~",";
    }
    F.writeln(_s);
    _s="Final States ";
    foreach(q;this.f_states)
    {
        _s~=q.name~",";
    }
    F.writeln(_s);
    F.writeln("transitions");

    foreach(t;this.rules)
    {
        _s="";
        int i=0;
        while(t[i]!=','){i++;}
        _s~=t[0..i]~"(";
        int j=t.length-2;
        while(t[j]!=','){j--;}
        int k=t.length;
        if(i!=j){ _s~=t[i+1..j]~") -> "~t[j+1..k-1];}else{_s~=") -> "~t[j+1..k-1];}

        F.writeln(_s);
    }


    F.close();
}

//Verify if a state is final

bool isFinalState (State st, State[] fs){
 bool final_st=true;
 foreach(s; fs){if(st != s){ final_st=false; break;} }
 return final_st;
}

}
