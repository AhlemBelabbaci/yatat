/*
    This module contains  functions that move through a Thompson 
    Tree automaton using a symbol or an epsilon transition
     
    Developed by Ahlem Belabbaci ah.belabbaci@lagh-univ.dz
     
*/

module src.basic.acceptance;

import src.basic.FTA;
import src.basic.essential;
import std.conv;
import std.stdio;
import std.string;
import std.algorithm;
import std.range;

// Functions that skip epsilon transitions

State [] skip_epsilon (State [] P, FTA F){

State [] R=P;
State [] X=null;
string [] rules= F.getRules();
while(R){
 X~=lambda(R, F); 
 R=epsilon(R, F); 
}
return(X);
}

State [] lambda (State [] P, FTA F){ 
//Returns set of states containing the edge states i.e. states that don't lead to any epsilon state
string [] rules= F.getRules();
State s;
State [] l=null;  
foreach (p;P){
 if(F.isFinalState(p,F.getFinalStates())){s.name=p.name;  l~=s;} 
 foreach (r;rules){
  string []temp = vectorize_Rule(r);
  if((r[0]!='e')&&(member(p.name, temp))){s.name=p.name;  l~=s;}
 }
}
return(l);
}


State [] epsilon (State [] P, FTA F){ 
//Returns set of states after skipping all leading epsilon states 
State [] ep=null;
string [] rules= F.getRules();  
foreach (p;P){
 foreach (r;rules){  
  string []temp = vectorize_Rule(r);
  if(r[0]=='e'&& temp[1]==p.name){State s; s.name=temp[$-1]; ep~=s; } 
 } 
}
return(ep);
}

// Move through the automaton using a symbol f

State [] move (State [] P, FTA F, char f){

State [] R=null;
string [] rules= F.getRules();  
foreach (r;rules){
  if (r[0]==f) { 
   string []temp = vectorize_Rule(r);
    if(P==null){State s; s.name=temp[1]; R~=s; break; }
    else{
     int i=0; bool child=true; 
     while((child) &&(i<P.length)){if(P[i].name != temp[i+1]){child =false;} ++i;}
     if (child){State s; s.name=temp[$-1]; R~=s; }
     } 
  }
}
return(R);
}

//Test if a state is a member of a rule

bool member (string st, string[] rul){
bool isThere=false;
rul=rul[1..$-1];
foreach(r;rul){
 if(st==r){isThere=true; break;}
}
return(isThere);
}

