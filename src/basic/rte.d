/*
      This module contains  the description and construction of    
      a regular tree expression class from an xml file along 
      with its validation and  convertion to a postfix notation 

     Developed by Ahlem Belabbaci ah.belabbaci@lagh-univ.dz 
*/

module src.basic.rte;

import src.basic.essential;
import src.basic.FTA;
import std.conv;
import std.algorithm.searching :commonPrefix;
import std.stdio,std.string;
import std.xml;
import std.file;
import std.algorithm.searching : canFind;
import std.regex;
import std.conv;
import std.algorithm.comparison:equal;
import std.algorithm.setops;
import std.algorithm.comparison : equal;
import std.range;
import std.algorithm;

// Definition of the class of the regular tree expression

public class rte 
{
  private :
  
  string reg_tree_exp;      
  Alphabet [] rte_alphabet ;    


 public :
       this() {reg_tree_exp = "";} // Default regular tree expression constructor
       

     void setRte(string n){this.reg_tree_exp=n;}
     string getRte(){return this.reg_tree_exp;}
     void setRteAlphabet(Alphabet [] A){this.rte_alphabet=A;}
     Alphabet [] getRteAlphabet(){return this.rte_alphabet;}
     int [string] isArityR(){
      int [string] A;
      foreach(a; this.rte_alphabet){ A[a.name] = a.rank;}
      return(A);
     }

//print out the RTE 

void printRte()
       {
           writeln("\n The regular tree expression is : ",getRte());
           write("\n The underlying alphabet is ");
           foreach(c;getRteAlphabet){write("(",c.name,", ",c.rank, "), ");};
           
       }


// Constructor of a regular tree expression from an xml file
       this(string f)
       {
           string s = cast(string)std.file.read(f);
           auto xml = new DocumentParser(s);   
           xml.onStartTag["rte"] = (ElementParser xml)
               {
                   xml.onEndTag["alphabet"] = (in Element e) { this.rte_alphabet=extractAlphabet(e.text); };
                   xml.onEndTag["expression"] = (in Element e) { this.reg_tree_exp = e.text;};
                   xml.parse();
               };
      xml.parse();


       } 
       
//Export the regular tree expression into an xml file
 
 void exportXML (){
 
  write("\n Exporting the regular tree expression  into an xml file ... "); 
  File file=File("rte.xml", "w+");
  file.writeln("<?xml version=\"1.0\"?>");
  file.writeln("<regulartreeexpression>");
  file.writeln("<rte>");
  file.write("<alphabet>");
  foreach(a; this.rte_alphabet){file.write(a.name,",",a.rank,";");}
  file.writeln("</alphabet>");
  file.write("<expression>");
  file.write(this.reg_tree_exp);
  file.writeln("</expression>");
  file.writeln("</rte>");
  file.writeln("</regulartreeexpression>");
  file.close(); 
  writeln(" Done!");
 }

//----------------------Validate the constructed regular tree expression -----------------------------

void validateRte()
{
   int lp=0;  int rp=0;
    
      // Verify if there's at least one leaf symbol 
      
      bool isLeaf=false;
      foreach(s;this.rte_alphabet)
      {
          if (s.rank==0){isLeaf=true;}
      }
      
      if(!isLeaf){ writeln("\n Unvalid regular tree expression. There is not a single leaf symbol");}
     

      //Check parenthesis syntax

      bool validParenthesis = false;
      
       foreach(c ; this.reg_tree_exp)
        { 
          if (c == '('){++lp;} else{ if(c ==')'){++rp;}}
        }
        
        if(rp != lp){writeln("\n Unvalid regular tree expression. Check parenthesis syntax");} else {validParenthesis = true; }
        
      //Verify if each symbol matches its rank  

 
int [string] isSymbol =["(":0,")":0,",":0];     
int [string] isOperator =[".":1,"*":1, "+":2];     

string [] stack ;
bool match_rank=true;
string s2, s3;
if(validParenthesis && isLeaf){
foreach(c ; this.reg_tree_exp)
{
if(to!string(c) !in this.isArityR && to!string(c) !in isSymbol && to!string(c) !in isOperator){
    writeln("\n Symbol ",c, " does not figure in the alphabet or symbols of this regular tree expression "); match_rank=false; break;} 
else{
   if((to!string(c) in this.isArityR) || ( c=='(')) {stack~=to!string(c); }
    else { 
      if(c==')'){ int comp=0; s2=""; s3="";
        while((s2 != "(")&&(stack != null))
        { ++comp; s2= stack.back; stack.popBack(); s3~=s2;
        }; --comp;
        if (stack != null && stack.back in isArityR ) {stack~=s3; s2= stack.back; stack.popBack(); }
        int x;
        if(s2 in isArityR && comp != this.isArityR.get(to!string(s2),x) ){writeln("\n Symbol ",s2, " doesnt match its rank"); match_rank=false; break;}
      }
     }
  }
}
}
 if (match_rank && validParenthesis &&  isLeaf){writeln("\n Valid regular tree expression!");}
 

}

string [] postfixRte() //Convert a regular tree expression to its postfix representation
{
  
int x;
int [string] isSymbol =["(":0,")":0,",":0];     
int [string] isOperator =[".":1,"*":1, "+":2];     
string [] stack, postfix, queue ;
bool child, star, plus, concat, end;
string s, psym, csym, ssym;

foreach(c; this.reg_tree_exp ){
 if(c==')'){  
  queue=null;    postfix=null;
  while(stack.back != "("){
   s= stack.back; stack.popBack();queue~=s; 
  }   
   assert(stack!=null, "Trying to pop an empty stack");
   stack.popBack(); //pop the opening parenthesis 

  if(stack!=null && this.isArityR.get(stack.back,x)>0){
   for(int i=1; i<= this.isArityR.get(stack.back,x);++i){
    postfix~= queue.back; queue.popBack(); 
   } 
   postfix~= stack.back ; 
   stack.popBack(); stack~=postfix; 
  }else{
   if(stack!= null && stack.back=="+"){
    assert(stack!=null, "Trying to pop an empty stack"); psym= stack.back; stack.popBack(); 
    assert(psym=="+", "Unvalid regular tree expression: consider adding parenthesis to the union second hand"); //pop the plus symbol
    while(queue != null){
     postfix~= queue.back;
     queue.popBack();
    } 
    stack~=postfix; stack~=psym;  
   }else{
     if(stack!= null && (stack.back in this.isArityR) && this.isArityR.get(stack.back,x)==0){
      assert(stack!=null, "Trying to pop an empty stack"); s= stack.back; stack.popBack(); //pop the concatenation symbol
      assert(stack!=null, "Trying to pop an empty stack"); csym= stack.back;  stack.popBack(); //pop the concatenation point
      assert(csym==".", "Unvalid regular tree expression: consider adding parenthesis to the concatenation second hand");
      csym=csym~s;
      while(queue != null){
       postfix~= queue.back;
       queue.popBack();
      }  
      stack~=postfix; stack~=csym; 
     }else{  
       while(queue != null){
        postfix~= queue.back;
        queue.popBack();
       } 
       stack~=postfix;     
    }
     }
   } 
 }
 else{if(c=='*'){ stack~=to!string(c); star=true; postfix=null;}
  else{
   if(c!=','){stack~=to!string(c); 
    if(stack!= null&& (to!string(c) in this.isArityR) && this.isArityR.get(stack.back,x)==0 && star){
     assert(stack!=null, "Trying to pop an empty stack"); 
     s=stack.back;  stack.popBack();//pop the closure symbol
     ssym= stack.back;  stack.popBack();  //pop the closure star
     assert(ssym=="*", "Unvalid regular tree expression: consider adding parenthesis to the closure second hand");
     postfix~=ssym~s;      
     stack~=postfix; 
     star=false;
    }
   }
 }}
}
return(stack);
}

}//end of class rte
