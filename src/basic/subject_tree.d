/*               
     This module contains  the description and construction of 
     a subject tree class from an xml file along with its validation,
     marking and linearization.
     
     Developed by Ahlem Belabbaci ah.belabbaci@lagh-univ.dz 
*/

module src.basic.subject_tree;

import src.basic.essential;
import src.basic.FTA;
import std.conv;
import std.range;
import std.stdio,std.string;
import std.xml;
import std.algorithm;
import std.file;


//Definition of the class of a subject tree
public class subject_tree 
{
  private :
    string subj_tree;
    Alphabet [] tree_alphabet ; 

  public :
       this() { subj_tree = ""; } // Default subject tree constructor
       
     //Methods for setting and getting attributs of a subject tree 
     
     void setSubjectTree(string n){subj_tree=n;}
     string getSubjectTree(){return subj_tree;}
     void setSubjectTreeAlphabet(Alphabet [] A){this.tree_alphabet=A;}
     Alphabet [] getSubjectTreeAlphabet(){return this.tree_alphabet;} 

     //Classifying the subject tree alphabet into a hash table
     int [string] isArityT(){
      int [string] A;
      foreach(a; this.tree_alphabet){ A[a.name] = a.rank;}
      return(A);
     }
     
     //print out the subject tree  

      void printSubjTree()
       {
           writeln("\n The subject tree is : ",getSubjectTree());
           write("\n The underlying alphabet is ");
           foreach(c;getSubjectTreeAlphabet()){write("(",c.name,", ",c.rank, "), ");};
           writeln("\n");
       }
    
  //Constructor of an subject tree from an xml file
  
       this(string f)
       {
           string s = cast(string)std.file.read(f);
           auto xml = new DocumentParser(s);   
           xml.onStartTag["st"] = (ElementParser xml)
               {   
                   xml.onEndTag["alphabet"] = (in Element e) { this.tree_alphabet=extractAlphabet(e.text); };
                   xml.onEndTag["subjtree"] = (in Element e) { this.subj_tree= e.text;};
                   xml.parse();
               };
      xml.parse();
      }   
 //Export the subject tree into an xml file
 
 void exportXML (){
 
  write("\n Exporting the subject tree into an xml file ... "); 
  File file=File("subj.xml", "w+");
  file.writeln("<?xml version=\"1.0\"?>");
  file.writeln("<subjecttree>");
  file.writeln("<st>");
  file.write("<alphabet>");
  foreach(a; this.tree_alphabet){file.write(a.name,",",a.rank,";");}
  file.writeln("</alphabet>");
  file.write("<subjtree>");
  file.write(this.subj_tree);
  file.writeln("</subjtree>");
  file.writeln("</st>");
  file.writeln("</subjecttree>");
  file.close(); 
  writeln(" Done!");
 }
 
//Validate the constructed subject tree


void validateSubjectTree()
{
int lp=0;  int rp=0;
    
      // Test if there's at least one leaf symbol 
      
      bool isLeaf=false;
      foreach(s;this.tree_alphabet)
      {
          if (s.rank==0){isLeaf=true; break;}
      }
      
      if(!isLeaf){ writeln("\n Unvalid subject tree. There should be at least one leaf symbol");}
     

      //Check parenthesis syntax

      bool validParenthesis = false;
      
       foreach(c ; this.subj_tree)
        { 
          if (c == '('){++lp;} else { if(c ==')'){++rp;}}
        }
        
        if(rp != lp){writeln("\n Unvalid subject tree. Check parenthesis syntax");}else {validParenthesis = true; }
        
      //Verify if each symbol matches its rank  


int [string] isSymbol =["(":0,")":0,",":0,".":1,"*":1, "+":2];     

string [] stack ;
bool match_rank=true;
string s2, s3;
if(validParenthesis && isLeaf){
foreach(c ; this.subj_tree)
{  if(to!string(c) !in this.isArityT && to!string(c) !in isSymbol){
    writeln("\n Symbol ", c, " does not figure in the alphabet of this subject tree. "); match_rank=false; break;} else{
   if((to!string(c) in this.isArityT) || ( c=='(')) {stack~=to!string(c);}
    else { 
      if(c==')'){ int comp=0; s2=""; s3="";
        while((s2 != "(")&&(stack != null))
        { ++comp; s2= stack.back; stack.popBack(); s3~=s2;
        }; --comp;
        if (stack != null && stack.back in isArityT ) {stack~=s3; s2= stack.back; stack.popBack(); }
        int x;
        if(s2 in isArityT && comp != this.isArityT.get(to!string(s2),x) ){writeln("\n Symbol ",s2, " does not match its rank"); match_rank=false; break;}
      }
     }
  }
}}
 if (match_rank &&validParenthesis &&isLeaf ){writeln("\n \nValid subject tree! ");}

        
}

//Mark and linearize the subject tree 

string[] markLinearizeTree (){


int ar=1; 
string mark="";
string[]stack;
string ss="";
string[] markedTree,_markedTree, linearTree, tempmarkedTree;

foreach(c;this.subj_tree){
 if(to!string(c)==")"){ 
  ar=to!int(mark[$-1]-'0')-1; mark= mark[0..$-1]; ss=""; 
  while(ss!= "(" ){ if(ss!=""){tempmarkedTree~=ss;} ss= stack.back; stack.popBack();} 
   while(tempmarkedTree!=null){
    markedTree~=tempmarkedTree.back; tempmarkedTree.popBack();
   }
  }
  else{
   if(to!string(c) in this.isArityT){mark~=to!string(ar); stack~=to!string(c)~mark;}else
   {if(to!string(c)=="("){ar= 1;stack~=to!string(c);  }else{
     if(to!string(c)==","){ar=to!int(mark[$-1]-'0')+1; mark= mark[0..$-1];} 
    }
   }
  }
}
int x; _markedTree=markedTree; linearTree=null;
foreach(t; _markedTree){ if(this.isArityT.get(to!string(t[0]),x)==0){linearTree~=t;}}
foreach(t; markedTree){
 foreach(tt; _markedTree){if(t[1..$-1]== tt[1..$]){ if(!(inArray (tt,linearTree))){linearTree~=tt;}}}
} 
return (linearTree);
}

bool inArray (string st, string [] S){
bool exist=false;
foreach(s;S){
 if(st==s){exist=true; break;}
}

return(exist);
}

}
