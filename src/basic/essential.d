/*
    This module contains  main functions and structures of automaton 
     
    Developed by Younes Guellouma y.guellouma@lagh-univ.dz     
*/


module src.basic.essential;

import std.string;
import std.regex;
import std.stdio;
import std.conv;
import std.algorithm.searching :commonPrefix;

//--------Alphabet definition, a ranked symbol is defined with a name and an integer rank
struct Alphabet
{
    string name;
    int rank;

    public :
        string getName(){return this.name;}
        int getRank(){return this.rank;}
        void setName(string n){name=n;}
        void setRank(int r){rank=r;}
};

//---------------signature defintion, it will be used in states definition
struct signature
{
    int occurence;
    int rule_number;
};

//-----------------State definition 
struct State
{
    string name;
    signature [] sig;
    string [] sigma_q;
    string [] output;// to mention the output of every new alphabet

};
// Definition of set of states, needed in tree pattern matching

struct SetofStates
{
    string name, node;
    State [] setst; 

};
//----------An alphabet is read as a succession of couples of (name,rank), this method recover the alphabet from a string s
Alphabet [] extractAlphabet(string s)
{
    Alphabet [] temp;  //return array
    Alphabet _temp;    //recover every instance
    string [] temp_name;    //recover symbols names list
    int [] temp_rank;     //recover symbols ranks list

    auto r=regex(r"([a-z]|[0-9]|[A-Z])*,");  //extract couples from s, a symbol's name contains only digits or normal characters
      foreach(c; matchAll(s, r))
      {
          string h = c.hit;
          h= removechars(h,",");
          temp_name~=h;
      }

      auto r2=regex(r"([0-9])*;");
      foreach(c; matchAll(s, r2))
      {
          string h = c.hit;
          h= removechars(h,";");
          temp_rank~=to!int(h);

      }
    if (temp_rank.length!=temp_name.length) //---if the names and the ranks number are not equal then the input is not well formatted
    {
        writeln("Error formating xml source");
    }
    else
    {


        for(int i=0;i<temp_rank.length;i++)
     {
        _temp.setName(temp_name[i]);
        _temp.setRank(temp_rank[i]);
        temp~=_temp;

     }

    }


    return temp;
}
//----------the same previous method but for states ---------------------
State [] extractStates(string s)
{
    State [] temp;
    State _temp;
    auto r=regex(r"([a-z]|[0-9]|[A-Z])*,");
    foreach(c; matchAll(s, r))
      {
          string h = c.hit;
          h= removechars(h,",");
          _temp.name=h;
          temp~=_temp ;
      }
       return temp;

}
//-----------------Compute an array containing states names
string[] getStatesNames(State[] s)
{
  string [] _temp;
  foreach(st;s)
  {
      _temp~=st.name;
  }
  return _temp;
}

string [] vectorize_Rule(string r)
{
   string [] _temp;
   //writeln(r);
   auto rex=regex(r"([a-z]|[0-9]|[A-Z])*,");
    foreach(c; matchAll(r, rex))
      {
          string h = c.hit;
          h= removechars(h,",");
          _temp~=h;
      }

    return _temp;
}

string compute_sigma(string [] vect)
{
    string _temp="";
    for(int i=0;i<=vect.length-2;i++)
    {
        _temp~=vect[i]~",";

    }

    return _temp;
}

Alphabet [] extract_Ops(string line)
{
    Alphabet []_temp;
    Alphabet symb;
    string [] _temp_names;
    int [] _temp_rank;
    if (commonPrefix(line,"Ops"))
    {
        auto rex=regex(r"([a-z]|[0-9]|[A-Z])+:");
        foreach(c; matchAll(line, rex))
      {
          string h = c.hit;
          h= removechars(h,":");
          _temp_names~=h;
      }
      //writeln(_temp_names);
       auto rex2=regex(r":([0-9])+");
        foreach(c; matchAll(line, rex2))
      {
          string h = c.hit;
          h= removechars(h,":");
          _temp_rank~=to!int(h);
      }
      //writeln(_temp_rank);

    }
    for(int i=0;i<_temp_rank.length;i++)
     {
        symb.setName(_temp_names[i]);
        symb.setRank(_temp_rank[i]);
        _temp~=symb;

     }
    return _temp;

}

string [] remove_Doubles(string [] s)
{
    string [] _temp;
    //foreach(e;s)
    return _temp;
}

