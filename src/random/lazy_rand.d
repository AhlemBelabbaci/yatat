
/*

 This module is dedicated to the random generation of tree automata
 
 Developed by Younes Guellouma y.guellouma@lagh-univ.dz 
*/

module src.random.lazy_rand;

import src.basic.FTA;
import src.basic.essential;
import std.random;
import std.conv;
import std.stdio;
import std.algorithm.searching : canFind;


FTA lazy_generate(int sigma_Size, int max_Rank, int q_Size, int delta_Size,string automaton_name)
{

    FTA targeted=new FTA();;
    Alphabet [] alphabet;
    State [] states;
    State [] f_states;
    string s="f";
    string q="q";
    string [] rules;
    //--------------generate alphabet---------------------
    
    for(int i=0;i<sigma_Size;i++)
    {
      auto _rank=uniform(0,max_Rank+1);
      Alphabet A;
      A.name=s~(to!string(i));
      A.rank=_rank;
      alphabet~=A;
    }

    //------------generate states-------------------------

    for(int i=0;i<q_Size;i++)
    {
       State state;
       state.name=q~(to!string(i));
       states~=state;
    }

   for (int i=0;i<delta_Size;i++)
    {

        string transition;
        auto pos=uniform(0,alphabet.length);
        transition~=alphabet[pos].name~",";
        for (int j=0;j<alphabet[pos].rank+1;j++)
        {
        auto _state=uniform(0,states.length);
        transition~=states[_state].name~",";
        }
        rules~=transition;

    }

    for (int i=0;i<q_Size;i++)
    {
        int k=uniform(0,2);
        if (k==0){f_states~=states[i];}
    }

    targeted.setName(automaton_name);
    targeted.setAlphabet(alphabet);
    targeted.setStates(states);
    targeted.setRules(rules);
    targeted.setFStates(f_states);

   return targeted;
}




FTA make_it_Det(FTA A)
{
    //----this function is to transform a FTA generated into a deterministic one. This is used when finding a FTA with transitions that match. Here, this functions builds a new FTA by doing arbitrary transformations when preserving states equivalences. Note that the new FTA is not the same as the  input one
    FTA _dfta=new FTA();
    State p,q,_q,_p;

    for(int i=0;i<A.getStates().length;i++)
    {
        p=A.getStates()[i];
        for(int j=i+1;j<A.getStates().length;j++)
        {q=A.getStates()[j];
        if ((A.are_Equivalent(p.sigma_q,q.sigma_q)))
        {
            //sort p.sigma and q_sigma then search for duplicated transitions and change symbol.
            //string _temp=p.getRules().sort;

        }
        }
    }
    return _dfta;

}

