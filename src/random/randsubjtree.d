/*
    This module contains  the function that generates a random 
    subject tree  containing a patterns of a regular tree expression
     
    Developed by Ahlem Belabbaci ah.belabbaci@lagh-univ.dz
     
*/

module src.random.randsubjtree;

import src.basic.rte;
import src.basic.subject_tree;
import src.basic.FTA;
import src.basic.essential;
import std.random;
import std.conv;
import std.range;
import std.stdio;
import std.algorithm.searching : canFind;

subject_tree randTree (rte R){
  string [] constructionStack, arityStack, starStack;
  string F, G;
  int i,x,j,k=0;
  Alphabet [] A; //Defining the subject tree's alphabet
  A~= R.getRteAlphabet();
  //Calculating the maximum ran for the alphabet A
  int max_rank=0;
  foreach(a; A){if(a.rank>max_rank){max_rank=a.rank;}}
   ++ max_rank;
  //Generating a random number of patterns
  
  foreach(s ; R.postfixRte()){
   if(s[0]=='*'){
    auto _star=uniform(0,11);
    if(_star==0){starStack~= to!string(s[1]); }else{
    for(j=0; j<_star; ++j ){
     starStack~= R.postfixRte()[0 .. k];
    }}
   }  ++k;
  }
  
  foreach(s ; starStack){
   if(s[0]=='+'){ //construct the union expression
    F=constructionStack.back; constructionStack.popBack();
    G=constructionStack.back; constructionStack.popBack();
    auto _union=uniform(1,3);
    if(_union==1){ constructionStack~=F;}
    else{constructionStack~=G;}
   }
   else{
    if(s[0]=='.'){ //construct the concatenation expression
     G=constructionStack.back; constructionStack.popBack();
     F=constructionStack.back; constructionStack.popBack();
     foreach(f;F){
      if(f==s[1]){ F=F.replace(to!string(f),G);}
      else{}
     }
     
     constructionStack~=F;
    }
     else{
      if(R.isArityR.get(s,x)==0){ //construct a leaf expression
       constructionStack~=s;
      }
      else{ //construct the arity expression
       arityStack=null; F="";
       for( i=1; i<= R.isArityR.get(to!string(s[0]),x);++i){arityStack~= constructionStack.back; constructionStack.popBack(); } 
       for( i=1; i<= R.isArityR.get(to!string(s[0]),x);++i){F~= arityStack.back~","; arityStack.popBack(); } 
       F=F[0..$-1];
       F= s[0]~"("~F~")";
       constructionStack~=F;
      }
     }
   }
  }

 //Generating the random subject tree 
 
 Alphabet al;  
 string Result="";
 int _rank, y;
 char _node;
 bool exist;
 
 exist=false;
  while(!exist){
   _node=uniform('a','z'); // Generate the first node of the subject tree
   y=inAlphabet(to!string(_node), A);
   if( y>0){
     _rank= y; //Quit the loop  
    exist=true; break;
   }else{
     if(y<0){
       _rank=uniform(1,max_rank); // Generate a rank for the first node 
      //Quit the loop 
      exist=true; break;
     }
    }
  }
 
 while(constructionStack.length>1){ 
  if(_rank <= constructionStack.length){ 
   //Adding the node to the subject tree alphabet
     if(y<0){ al.name=to!string(_node); al.rank=_rank; A~=al; }
   Result= to!string(_node)~"(";
   for(j=0; j<_rank; ++j){
     Result~=constructionStack.back~","; constructionStack.popBack();
   }   
    Result=Result[0 .. $-1]~")";
    constructionStack~=Result; Result="";
   } 
    //The same work as for generating first node
    
  exist=false;
  while(!exist){
   _node=uniform('a','z'); // Generate a node of the subject tree
   y=inAlphabet(to!string(_node), A); 
   if( y>0){
     _rank= y; 
    //Quit the loop  
    exist=true; break;
   }else{
     if(y<0){
       _rank=uniform(1,max_rank); // Generate a rank of for the previous node  
      //Quit the loop 
      exist=true; break;
     }
    }
  }
      
  
 }
 Result="";
 foreach(c; constructionStack){Result~=c;}

 //New subject tree instance
 
 auto randomtree= new subject_tree();
 Result="("~Result~")";
 randomtree.setSubjectTree(Result) ;
 randomtree.setSubjectTreeAlphabet(A);
 
 return(randomtree);
}

int inAlphabet (string s, Alphabet [] A){
int x;
bool exist=false;
foreach(a; A){ if(a.name==s){x = a.rank; exist=true; break;}}

if(exist){return(x);}else{return(-1);}

}
