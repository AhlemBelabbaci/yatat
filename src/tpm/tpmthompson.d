/*

    This module contains the tree pattern matching implementation
    using Thompson Tree automaton construction

    Developed by Ahlem Belabbaci ah.belabbaci@lagh-univ.dz 
*/

module src.tpm.tpmthompson;

import src.basic.subject_tree;
import src.basic.FTA;
import src.basic.essential;
import src.basic.acceptance;
import std.conv;
import std.stdio;
import std.string;
import std.algorithm;
import std.range;

string [] tpm (subject_tree Ot, FTA F){
//Ot is the subject tree and F is the thompson tree automaton of the pattern's regular tree expression
string [] R;

SetofStates []SoS;
State []st;

string[] subjt=Ot.markLinearizeTree ();
foreach(o; subjt){
 SetofStates P;
 P.name ="P"~o[1..$];
 P.node =o;
 int x;
 if(Ot.isArityT.get(to!string(o[0]),x)==0){
  P.setst~=skip_epsilon(move(null,F,o[0]),F) ;
 }
 else{
   st=null;
  foreach(s;SoS){ if(s.name[1..$-1]== o[1..$]){st~=s.setst;}}; 
   P.setst~=skip_epsilon(move(st,F,o[0]),F) ;
 }

 foreach(p;P.setst){ 
   if( F.isFinalState(p ,F.getFinalStates)){
     auto index = countUntil(P.setst, p);
     P.setst=remove(P.setst, index);
     R~=P.node;
   }
  }
 SoS~=P;
}
return(R);
}

