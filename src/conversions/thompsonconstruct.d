/*

    This module contains the different constructions of  
    Thompson Tree automaton from a regular tree expression 

    Developed by Ahlem Belabbaci ah.belabbaci@lagh-univ.dz  
*/

module src.conversions.thompsonconstruct;

import src.basic.rte;
import src.basic.FTA;
import src.basic.essential;
import std.conv;
import std.stdio;
import std.algorithm.searching : canFind;
import std.range;

//Adding a symbol to an alphabet
Alphabet [] addSymbol (Alphabet [] A, string s, int r){
bool exist=false;
foreach(a;A){ if(a.name == s){exist=true; break; }}

if(!exist){
 Alphabet a;
 a.name=s;
 a.rank=r;
 A~=a;
}
return(A);
}

//Adding a state to a set of states
State [] addState (State [] SSt, string s){
bool exist=false;
foreach(st;SSt){ if(st.name == s){exist=true; break; }}

if(!exist){
 State st;
 st.name=s;
 SSt~=st;
}
return(SSt);
}

// Thompson tree automaton of a leaf tree

FTA leaf_automaton (string state_name, char c){

 FTA leaf_aut = new FTA();
 Alphabet [] alphabet;
 State [] states;
 State state;
 State [] f_states;
 string [] rules;
 string rule;

 // Defining the alphabet of the leaf automaton
 
 alphabet=addSymbol(alphabet, to!string(c), 0);

 
 //Defining set of states and the final state of the leaf automaton
 states=addState(states,"q"~state_name);
 f_states=addState(f_states,"q"~state_name);
  
 //Defining the set of transitions of the leaf automaton
 
 rule = c~",q"~state_name~",";
 rules~= rule;  
 
 //writeln("Rules added in leaf automaton ", rules);
 //Constructing the leaf automaton
  
 leaf_aut.setAlphabet(alphabet);
 leaf_aut.setStates(states);
 leaf_aut.setRules(rules);
 leaf_aut.setFStates(f_states);

 return leaf_aut;

}


// Thompson tree automaton for arity

FTA arity_automaton (string state_name , char c, FTA[] F...){ // variable number of automata depending on the rank of the symbol f


 FTA arity_aut = new FTA();
 Alphabet [] alphabet;
 State [] states;
 State state;
 State [] f_states;
 string [] rules;
 string rule;
 
 
 // Defining the alphabet of the arity automaton
 
 foreach(f; F){ 
  foreach(a;f.getAlphabet()){
   alphabet=addSymbol(alphabet, a.name, a.rank);
   }}

 //Adding the arity symbol to the alphabet
 alphabet=addSymbol(alphabet, to!string(c), F.length);
 
 //Adding epsilon to the alphabet
 alphabet=addSymbol(alphabet, "e", 1);
 
 // Adding states of all automata F_i to set of states
 
 
 
 foreach(f; F){ 
  foreach(st; f.getStates()){
   states=addState(states,st.name);
  }
 }
 
 // Defining final state of the arity automaton
  
 f_states=addState(f_states,"q"~state_name);    
 //Adding final state to set of states
 
 states=addState(states,"q"~state_name);
 //Adding initial states to set of states
 
  foreach(f; F){
    foreach (a;f.getAlphabet()){
     if (a.rank == 0){ states=addState(states,"q"~state_name~a.name);}
    }
  }
  
 // Removing initial states' transitions from automata F_i and adding them to the arity automaton plus the epsilon transitions
 
 int [string] symb_rank;
 
 foreach(f; F){ 
  foreach (a;f.getAlphabet()) { symb_rank[a.name] = a.rank ; }
 }
 
 string [] f_rule;
 foreach(f; F){ 
  f_rule = f.getRules(); 
  foreach(r; f_rule){
    int y; 
    if(symb_rank.get(to!string(r[0]),y)>0) { rules~=r; } else {
    rules~= r[0]~",q"~state_name~r[0]~"," ; rules~= "e,q"~state_name~r[0]~","~r[2..r.length-1]~","; } 
   } // e stands for epsilon transition
 }
 

// Adding the transition of arity
 rule=to!string(c)~",";
 foreach(f;F){
   foreach(st; f.getFinalStates()){
    rule ~= st.name~","; 
   }
 }
  rule~="q"~state_name~",";
  rules~=rule; 
  
 //writeln("Rules added in arity automaton ", rules);
 
  // Constructing the arity automaton
 
 arity_aut.setAlphabet(alphabet);
 arity_aut.setStates(states);
 arity_aut.setRules(rules);
 arity_aut.setFStates(f_states);
 
 return arity_aut;
 
}

// Thompson tree automaton for union

FTA union_automaton (string state_name , FTA F, FTA G){

 FTA union_aut = new FTA();
 Alphabet [] alphabet;
 State [] states;
 State state;
 State [] f_states;
 string [] rules;
 string rule;
 
 // Defining the alphabet of the union automaton
 

  foreach(a;F.getAlphabet()){
   alphabet=addSymbol(alphabet, a.name, a.rank);
   }
   

  foreach(a;G.getAlphabet()){
   alphabet=addSymbol(alphabet, a.name, a.rank);
   }
  
  //Adding epsilon to the alphabet
  alphabet=addSymbol(alphabet, "e", 1);
 
 // Adding states of both automata F and G to set of states
 
 
  foreach(st; F.getStates()){
   states=addState(states,st.name);
  }
 
  foreach(st; G.getStates()){
   states=addState(states,st.name);
  }
 
 
 
 // Defining final state of the union automaton
 f_states=addState(f_states,"q"~state_name);   
 
    
 //Adding final state to set of states
 states=addState(states,"q"~state_name);
 
 //Adding initial states to set of states

  
  foreach (a;F.getAlphabet()){
     if (a.rank == 0){ states=addState(states,"q"~state_name~a.name);}
    }
  foreach (a;G.getAlphabet()){
     if (a.rank == 0){ states=addState(states,"q"~state_name~a.name);}
    } 
 
// Removing initial states' transitions from automata F and G 
//then adding them to the union automaton plus the epsilon transitions
 
 int [string] symb_rank;

  foreach (a;F.getAlphabet()){
      symb_rank[a.name] = a.rank ;
  }

  foreach (a;G.getAlphabet()){
      symb_rank[a.name] = a.rank ;
  }
  int y;
 string [] f_rule;
 f_rule = F.getRules(); 
  foreach(r; f_rule){
    if(symb_rank.get(to!string(r[0]),y)>0) { rules~=r; } else {
    rules~= r[0]~",q"~state_name~r[0]~"," ; rules~= "e,q"~state_name~r[0]~","~r[2..r.length-1]~","; } 
   } // e stands for epsilon transition
  
 f_rule = G.getRules(); 
  foreach(r; f_rule){    
    if(symb_rank.get(to!string(r[0]),y)>0) { rules~=r; }  else {
    rules~= r[0]~",q"~state_name~r[0]~"," ; rules~= "e,q"~state_name~r[0]~","~r[2..r.length-1]~","; } 
   } 

//Adding the epsilon transitions of union

 rule="e";
 foreach(st; F.getFinalStates()){rule ~= ","~st.name~",";}
 rule~="q"~state_name~","; 
 rules~=rule;

 rule="e";
 foreach(st; G.getFinalStates()){rule ~= ","~st.name~",";}
 rule~="q"~state_name~",";
 rules~=rule; 
 
 //writeln("Rules added in union automaton ", rules);

 // Constructing the union automaton
 
 union_aut.setAlphabet(alphabet);
 union_aut.setStates(states);
 union_aut.setRules(rules);
 union_aut.setFStates(f_states);
 
 return union_aut;

}

// Thompson tree automaton for concatenation

FTA concat_automaton (string state_name , FTA F, char c, FTA G){

 FTA concat_aut = new FTA();
 Alphabet [] alphabet;
 State [] states;
 State state;
 State [] f_states;
 string [] rules;
 string rule;
 
 // Defining the alphabet of the concatenation automaton
 

  foreach(a;F.getAlphabet()){
   alphabet=addSymbol(alphabet, a.name, a.rank);
   }
   

  foreach(a;G.getAlphabet()){
   alphabet=addSymbol(alphabet, a.name, a.rank);
   }
  
  //Adding epsilon to the alphabet
  alphabet=addSymbol(alphabet, "e", 1);
  
 // Adding states of both automata F and G to set of states
 
 foreach(st; F.getStates()){
   states=addState(states,st.name);
  }
 
  foreach(st; G.getStates()){
   states=addState(states,st.name);
  }
 
 
 // Defining final state of the concatenation automaton
 
   f_states=addState(f_states,"q"~state_name); 
    
 //Adding final state to set of states
   states=addState(states,"q"~state_name);
 
 //Adding initial states to set of states

 
  foreach (a;F.getAlphabet()){
     if (a.rank == 0){ states=addState(states,"q"~state_name~a.name);}
    }
  foreach (a;G.getAlphabet()){
     if (a.rank == 0){ states=addState(states,"q"~state_name~a.name);}
    } 
    
// Removing initial states' transitions from automata F and G then adding them to the concatenation automaton plus the epsilon transitions
 
 int [string] symb_rank;

  foreach (a;F.getAlphabet()){
      symb_rank[a.name] = a.rank ;
  }

  foreach (a;G.getAlphabet()){
      symb_rank[a.name] = a.rank ;
  }
     int y;
 string [] f_rule;
 f_rule = F.getRules(); 
  foreach(r; f_rule){   
    if(symb_rank.get(to!string(r[0]),y)>0) { rules~=r; } else {
     if(r[0]!=c){rules~= r[0]~",q"~state_name~r[0]~"," ; rules~= "e,q"~state_name~r[0]~","~r[2..r.length-1]~",";} else {
      rule="e";
      foreach(st; G.getFinalStates()){rule ~= ","~st.name~",";}
      rule~=r[2..r.length-1]~",";
      rules~=rule; 
     }
    } 
   } // e stands for epsilon transition
  
 f_rule = G.getRules(); 
  foreach(r; f_rule){
   if(symb_rank.get(to!string(r[0]),y)>0) { rules~=r;} else 
   {rules~= r[0]~",q"~state_name~r[0]~"," ; 
    rules~= "e,q"~state_name~r[0]~","~r[2..r.length-1]~",";} 
   } 

//Adding the epsilon transition of concatenation, from F to final state

 rule="e";
 foreach(st; F.getFinalStates()){rule ~= ","~st.name~",";}
 rule~="q"~state_name~",";
 rules~=rule;

 //writeln("Rules added in concatenation automaton ", rules);
 // Constructing the concatenation automaton
 
 concat_aut.setAlphabet(alphabet);
 concat_aut.setStates(states);
 concat_aut.setRules(rules);
 concat_aut.setFStates(f_states);
 
 return concat_aut;
}

// Thompson tree automaton for closure

FTA closure_automaton (string state_name, FTA F, char c){

 FTA closure_aut = new FTA();
 Alphabet [] alphabet;
 State [] states;
 State state;
 State [] f_states;
 string [] rules;
 string rule;
 
 // Defining the alphabet of the closure automaton
 
 foreach(a;F.getAlphabet()){
   alphabet=addSymbol(alphabet, a.name, a.rank);
   }
 
  //Adding epsilon to the alphabet
  alphabet=addSymbol(alphabet, "e", 1);

 
 // Adding states of automaton F to set of states
 
 foreach(st; F.getStates()){
   states=addState(states,st.name);
  }
 
 // Defining final state of the closure automaton
 
  f_states=addState(f_states,"q"~state_name); 
 //Adding final state to set of states
  states=addState(states,"q"~state_name);
 
 //Adding initial states to set of states

 
  foreach (a;F.getAlphabet()){
     if (a.rank == 0){ states=addState(states,"q"~state_name~a.name);}
    }
    
// Removing initial states' transitions from automata F and adding them to the closure automaton with the epsilon transitions
 
 int [string] symb_rank;
  foreach (a;F.getAlphabet()){
      symb_rank[a.name] = a.rank ;
  }
 string [] f_rule;
 f_rule = F.getRules(); 
  foreach(r; f_rule){
    int y; 
    if(symb_rank.get(to!string(r[0]),y)>0) { rules~=r; } else 
    {rules~= r[0]~",q"~state_name~r[0]~"," ; rules~= "e,q"~state_name~r[0]~","~r[2..r.length-1]~",";  
     if(r[0]==c){ // Adding the epsilon transitions of closure
      rule="e"; 
      foreach(st; F.getFinalStates()){rule ~= ","~st.name~",";}
      string s = rule~r[2..r.length-1]~",";
      rules~=s; 
      s= "e,q"~state_name~r[0]~",q"~state_name~","; 
      rules~=s; 
     }
    } 
   } // e stands for epsilon transition
  
//Adding the epsilon transitions of closure, from F to final state
 rule="e";
 foreach(st; F.getFinalStates()){rule ~= ","~st.name~",";}
 rule~="q"~state_name~",";
 rules~=rule; 
 
  // Constructing the closure automaton
 
 closure_aut.setAlphabet(alphabet);
 closure_aut.setStates(states);
 closure_aut.setRules(rules);
 closure_aut.setFStates(f_states);
 
 return closure_aut;
}
//Constructing an FTA from a regular tree expression

FTA RTEtoFTA(rte R)
{
  FTA [] constructionStack, arityStack1, arityStack2;
  FTA F= new FTA(); FTA G= new FTA(); 
  int x, st_name=0;
   
  foreach(s ; R.postfixRte()){
   ++st_name;
   if(s[0]=='+'){ //Construct the union automaton
    F=constructionStack.back; constructionStack.popBack();
    G=constructionStack.back; constructionStack.popBack();
    F=union_automaton (to!string(st_name),F,G);
    constructionStack~=F;
   }
   else{
    if(s[0]=='.'){ //Construct the concatenation automaton
     G=constructionStack.back; constructionStack.popBack();
     F=constructionStack.back; constructionStack.popBack();
     F=concat_automaton (to!string(st_name), F, s[1] ,G );
     constructionStack~=F;
    }
    else{
     if(s[0]=='*'){ //Construct the closure automaton
      F=constructionStack.back; constructionStack.popBack();
      F=closure_automaton (to!string(st_name), F, s[1]);
      constructionStack~=F;     
     }
     else{
      if(R.isArityR.get(s,x)==0){ //Construct a leaf automaton
       F=leaf_automaton( to!string(st_name),s[0]);
       constructionStack~=F;
      }
      else{ //Construct the arity automaton
       arityStack1=null; arityStack2=null;
       for(int i=1; i<= R.isArityR.get(to!string(s[0]),x);++i){arityStack1~= constructionStack.back; constructionStack.popBack(); } 
       for(int i=1; i<= R.isArityR.get(to!string(s[0]),x);++i){arityStack2~= arityStack1.back; arityStack1.popBack(); } 
       F= arity_automaton (to!string(st_name) , s[0] ,arityStack2);
       constructionStack~=F;
      }
     }
    }
   }
  }

return(F);
}
