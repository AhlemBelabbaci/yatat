/*

    This module contains the synthesis of a regular tree
     expression from a tree automaton  using integrals

    Developed by Ahlem Belabbaci ah.belabbaci@lagh-univ.dz  
*/

module src.conversions.integrals;

import src.basic.rte;
import src.basic.FTA;
import src.basic.essential;
import std.conv;
import std.stdio;
import std.algorithm.searching : canFind;
import std.range;

//Adding a symbol to the RTE
rte addSymbolRTE (rte A, string s, int r){
bool exist=false;
foreach(a;A){ if(a.name == s){exist=true; break; }}

if(!exist){
 Alphabet a;
 a.name=s;
 a.rank=r;
 A~=a;
}
return(A);
}

//Adding an operator to the RTE
rte [] addOpRTE (rte SSt, string s){
bool exist=false;
foreach(st;SSt){ if(st.name == s){exist=true; break; }}

if(!exist){
 State st;
 st.name=s;
 SSt~=st;
}
return(SSt);
}


//Function deducing equations from tree automaton states



//Synthesis of a regular tree expression from a tree automaton using integrals

FTA PositionRTEtoFTA(rte R)
{
  rte R= new rte(); 
  
  foreach(s ; R.postfixRte()){
  }
  

return(F);
}
