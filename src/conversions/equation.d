/*

    This module contains the construction of  the Equation 
    tree automaton from a regular tree expression 

    Developed by Ahlem Belabbaci ah.belabbaci@lagh-univ.dz  
*/

module src.conversions.equation;

import src.basic.rte;
import src.basic.FTA;
import src.basic.essential;
import std.conv;
import std.stdio;
import std.algorithm.searching : canFind;
import std.range;

//Adding a symbol to an alphabet
Alphabet [] addSymbol (Alphabet [] A, string s, int r){
bool exist=false;
foreach(a;A){ if(a.name == s){exist=true; break; }}

if(!exist){
 Alphabet a;
 a.name=s;
 a.rank=r;
 A~=a;
}
return(A);
}

//Adding a state to a set of states
State [] addState (State [] SSt, string s){
bool exist=false;
foreach(st;SSt){ if(st.name == s){exist=true; break; }}

if(!exist){
 State st;
 st.name=s;
 SSt~=st;
}
return(SSt);
}


//Derivatives function


//Constructing an FTA from a regular tree expression using derivates

FTA PositionRTEtoFTA(rte R)
{
  FTA F= new FTA(); 
  
  foreach(s ; R.postfixRte()){
  }
  
  // Defining the alphabet of the equation automaton
 
   alphabet=addSymbol(alphabet, to!string(c), 0);

 
 //Defining set of states and the final state of the equation automaton
   
   states=addState(states,"q"~state_name);
   f_states=addState(f_states,"q"~state_name);
  
 //Defining the set of transitions of the equation automaton
 
   rule = c~",q"~state_name~",";
   rules~= rule;  
 
 //Constructing the equation automaton
  
 F.setAlphabet(alphabet);
 F.setStates(states);
 F.setRules(rules);
 F.setFStates(f_states);

return(F);
}
