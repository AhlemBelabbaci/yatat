
/*
    This is an example of tree pattern matching using  Thompson tree 
    automaton construction
    
    Developed by Ahlem Belabbaci ah.belabbaci@lagh-univ.dz
     
*/

import src.basic.FTA;
import src.basic.rte;
import std.stdio,std.string;
import src.tpm.tpmthompson;
import src.conversions.thompsonconstruct;
import src.basic.subject_tree;

void main (string [] argv)
{
//Reading the regular tree expression as first argument 
auto expression = new rte(argv[1]);
 if (argv.length > 1) {
  writeln("\n");
  writeln("***********************************");
  writeln(" The input regular tree expression ");
  writeln("***********************************");
  expression.printRte(); }
 else 
  { writeln("\n No xml found for this regular tree expression "); }
  
  string [] s=expression.postfixRte();
  writeln("\n\n The postfix form of the regular tree expression is \n\n ",s);
  expression.validateRte();
 
//Reading the subject tree as second argument
auto subjecttree = new subject_tree(argv[2]);
 if (argv.length > 2) {
  writeln("\n");
  writeln("***********************");
  writeln(" The input subject tree ");
  writeln("***********************");
  subjecttree.printSubjTree();
  subjecttree.validateSubjectTree();
  writeln("\n The marked and linearized subject tree is \n\n ", subjecttree.markLinearizeTree()); }
 else 
  { writeln("\n No xml found for this subject tree"); }
    
  //Constructing Thompson tree automaton
  writeln("\n");
  writeln("*********************************************************************");
  writeln(" Constructing Thompson Tree Automaton of the regular tree expression ");
  writeln("*********************************************************************");
  auto automate = new FTA();
  automate=RTEtoFTA(expression);
  automate.setName("Thompson Tree Automaton");
  automate.visualisate();
  automate.exportXML();
 //Performing the tree pattern matching
 
  writeln("\n");
  writeln("*************************************************************");
  writeln(" Performing tree pattern matching on Thompson Tree Automaton ");
  writeln("*************************************************************");
 
s= tpm (subjecttree, automate);
if(s!=null){writeln("\n This subject tree matches the pattern's regular tree expression in the folowing nodes:",s);}else
{writeln("\n No matches found for this subject tree");}
 
 
 
}
