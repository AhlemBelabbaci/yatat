
/*
    This is an example of testing the validity of tree automaton, 
    regular tree expression, and subject tree
     
    Developed by Ahlem Belabbaci ah.belabbaci@lagh-univ.dz
     
*/

import src.basic.FTA;
import src.basic.rte;
import src.basic.subject_tree;
import std.stdio,std.string;

void main (string [] argv)
{
//testing FTA.d
 auto automate = new FTA(argv[1]);
 automate.setName("My first automaton");

 if (argv.length > 1) {
   automate.visualisate(); }
 else 
  { writeln(" No xml found for this automata "); }
 
//testing rte.d
auto expression = new rte(argv[2]);
 if (argv.length > 2) {
   expression.printRte(); }
 else 
  { writeln(" No xml found for this regular tree expression "); }
  
  expression.validateRte();
  string [] s=expression.postfixRte();
  string post;
  foreach(x;s){post~=x;}
  writeln("The postfix notation of this regular tree expression is: ", post);
  
//testing subject_tree.d
auto subjecttree = new subject_tree(argv[3]);
 if (argv.length > 3) {
   subjecttree.printSubjTree(); }
 else 
  { writeln(" No xml found for this subject tree"); }
  
  
  subjecttree.validateSubjectTree();
  s=subjecttree.markLinearizeTree ();
  writeln("The marked and linearized subject tree is ", s);
}
