/*
    This is an example of constructing a Thompson tree automaton 
    from a regular tree expression read as an input parameter
     
    Developed by Ahlem Belabbaci ah.belabbaci@lagh-univ.dz
     
*/

import src.basic.FTA;
import src.basic.rte;
import std.stdio,std.string;
import src.conversions.thompsonconstruct;

void main (string [] argv)
{
//Reading the regular tree expression as first argument 
auto expression = new rte(argv[1]);
 if (argv.length > 1) {
   expression.printRte(); }
 else 
  { writeln(" No xml found for this regular tree expression "); }
  
  expression.validateRte();
  string [] s=expression.postfixRte();
  writeln(s);
 //Creating the automaton 
 
  auto automate = new FTA();
  automate=RTEtoFTA(expression);
  automate.setName("My first automaton construction");
  automate.visualisate();
}
