/*
    This is an example of testing the validity of random subject tree
     
    Developed by Ahlem Belabbaci ah.belabbaci@lagh-univ.dz
     
*/
import src.basic.rte;
import src.basic.subject_tree;
import std.stdio,std.string;
import src.random.randsubjtree;

void main (string [] argv)
{
//Reading the regular tree expression as first argument 
auto expression = new rte(argv[1]);
 if (argv.length > 1) {
  writeln("\n");
  writeln("***********************************");
  writeln(" The input regular tree expression ");
  writeln("***********************************");
  expression.printRte(); }
 else 
  { writeln("\n No xml found for this regular tree expression "); }
  
  string [] s=expression.postfixRte();
  writeln("\n\n The postfix form of the regular tree expression is \n\n ",s);
  expression.validateRte();
  
  
  auto randomSubjTree= new subject_tree();
  randomSubjTree=randTree(expression);
  randomSubjTree.printSubjTree;
  randomSubjTree.validateSubjectTree();
  writeln("\n The marked and linearized subject tree is \n\n ", randomSubjTree.markLinearizeTree());
  randomSubjTree.exportXML();

}
