# makefile to compile, build and test YATAT
#Author: Ahlem Belabbaci ah.belabbaci@lagh-univ.dz

#To compile all examples in the 'examples' directory use 'make all'
# To delete all the generated files use 'make clean'

OPTION=-c
SOURCE=/home/ahlem/Desktop/YATAT/src/
EXAMPLES=/home/ahlem/Desktop/YATAT/examples/
BIN=-od=/home/ahlem/Desktop/YATAT/bin/
D=dmd
RM=/bin/rm -f

all: constructionTest.o Testrandsubjtree.o tpmTest.o ValidityTest.o

thompsonconstruct.o: $(SOURCE)thompsonconstruct.d rte.o FTA.o essential.o sa.o
	$(D) $(OPTION) $(BIN) $(SOURCE)thompsonconstruct.d $(SOURCE)rte.d $(SOURCE)FTA.d $(SOURCE)essential.d $(SOURCE)sa.d 

acceptance.o: $(SOURCE)acceptance.d FTA.o sa.o essential.o sa.o
	$(D) $(OPTION) $(BIN) $(SOURCE)acceptance.d $(SOURCE)essential.d $(SOURCE)FTA.d $(SOURCE)sa.d

tpmthompson.o: $(SOURCE)tpmthompson.d subject_tree.o FTA.o essential.oT acceptance.o sa.o
	$(D) $(OPTION) $(BIN) $(SOURCE)tpmthompson.d $(SOURCE)subject_tree.d $(SOURCE)FTA.d $(SOURCE)essential.d $(SOURCE)acceptance.d

subject_tree.o:  $(SOURCE)subject_tree.d essential.o FTA.o sa.o
	$(D) $(OPTION) $(BIN) $(SOURCE)subject_tree.d $(SOURCE)essential.d $(SOURCE)FTA.d $(SOURCE)sa.d

rte.o: $(SOURCE)rte.d FTA.o essential.o acceptance.o sa.o
	$(D) $(OPTION) $(BIN) $(SOURCE)rte.d $(SOURCE)essential.d $(SOURCE)FTA.d $(SOURCE)sa.d

essential.o:
	$(D) $(OPTION) $(BIN) $(SOURCE)essential.d $(SOURCE)FTA.d $(SOURCE)sa.d

sa.o:
	$(D) $(OPTION) $(BIN) $(SOURCE)sa.d $(SOURCE)essential.d $(SOURCE)FTA.d 
	
FTA.o: 
	$(D) $(OPTION) $(BIN) $(SOURCE)FTA.d $(SOURCE)sa.d $(SOURCE)essential.d
	
randsubjtree.o: $(SOURCE)randsubjtree.d subject_tree.o FTA.o essential.o acceptance.o sa.o
	$(D) $(OPTION) $(BIN) $(SOURCE)randsubjtree.d $(SOURCE)rte.d $(SOURCE)subject_tree.d $(SOURCE)FTA.d $(SOURCE)essential.d $(SOURCE)sa.d

#Test examples  

constructionTest.o: $(EXAMPLES)constructionTest.d thompsonconstruct.o acceptance.o rte.o sa.o
	$(D) $(BIN) $(EXAMPLES)constructionTest.d $(SOURCE)FTA.d $(SOURCE)essential.d $(SOURCE)rte.d $(SOURCE)thompsonconstruct.d $(SOURCE)acceptance.d $(SOURCE)sa.d

Testrandsubjtree.o:  $(EXAMPLES)testrandsubjtree.d randsubjtree.o rte.o sa.o 
	$(D) $(BIN) $(EXAMPLES)testrandsubjtree.d $(SOURCE)rte.d $(SOURCE)subject_tree.d $(SOURCE)randsubjtree.d $(SOURCE)FTA.d $(SOURCE)essential.d $(SOURCE)sa.d


tpmTest.o:  $(EXAMPLES)tpmTest.d thompsonconstruct.o tpmthompson.o subject_tree.o rte.o acceptance.o
	$(D) $(BIN) $(EXAMPLES)tpmTest.d  $(SOURCE)FTA.d $(SOURCE)rte.d $(SOURCE)tpmthompson.d $(SOURCE)thompsonconstruct.d $(SOURCE)subject_tree.d $(SOURCE)essential.d $(SOURCE)sa.d $(SOURCE)acceptance.d 


ValidityTest.o:  $(EXAMPLES)ValidityTest.d subject_tree.o rte.o 
	$(D) $(BIN) $(EXAMPLES)ValidityTest.d $(SOURCE)FTA.d $(SOURCE)rte.d $(SOURCE)subject_tree.d $(SOURCE)essential.d $(SOURCE)sa.d

clean:
	$(RM) *.o 

